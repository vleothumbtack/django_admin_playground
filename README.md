# Инструкция по установке

- ставим питон 3.6
- git clone git@bitbucket.org:vleothumbtack/django_admin_playground.git && cd django_admin_playground
- python3 -m pip install --user virtualenv
- python3 -m venv env
- cd env/bin && source activate
- cd ../.. && pip install -r requirements.txt
- python manage.py migrate
- python manage.py createsuperuser -> создаем админа интерактивно
- python manage.py runserver
- localhost:8000/admin -> креды админа -> откроется админка