import os

from django.db import models
from django.contrib.auth.models import User

from django.conf import settings


class Image(models.Model):
    name = models.CharField(max_length=100)
    file = models.ImageField()

    def url(self):
        return os.path.join('/', settings.MEDIA_URL, os.path.basename(str(self.file)))

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=100)
    image = models.ForeignKey(Image, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Article(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    category = models.ManyToManyField(Category)
    gallery = models.ManyToManyField(Image)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
