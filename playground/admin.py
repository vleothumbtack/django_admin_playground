from django.contrib import admin

from reversion.admin import VersionAdmin

from .models import Category, Image, Article


class TunedArticle(VersionAdmin):
    fieldsets = (
        ('Common', {
            'fields': ('name', 'gallery', 'owner')
        }),
        ('Site-specific', {
            'classes': ('collapse',),
            'fields': ('category',)
        }),
    )


admin.site.register(Image, VersionAdmin)
admin.site.register(Category, VersionAdmin)
admin.site.register(Article, TunedArticle)
